/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.services;

import java.util.Optional;

import javax.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.api.models.EfUser.Eca;
import org.eclipsefoundation.profile.models.GerritResponse;
import org.eclipsefoundation.profile.models.PeopleProject;
import org.eclipsefoundation.profile.models.ProfileAPISearchParams;
import org.eclipsefoundation.profile.models.Subscriptions;

/**
 * Defines the Profile service for fetching a user's profile data.
 */
public interface ProfileService {

    /**
     * Fetches a user profile given the username. Returns a slimmed down version that does not contain the relationship urls and
     * eca/committer status. This is used to access some basic information about a user while performing the minimal amount of API requests.
     * 
     * @param username The given user's EF username.
     * @return An Optional conatining an EfUser if it exists.
     */
    Optional<EfUser> getSlimProfileByUsername(String username);

    /**
     * Fetches a user profile given the username. Aggregates ECA and committer status information.
     * 
     * @param username The given user's EF username.
     * @return An Optional conatining an EfUser if it exists.
     */
    Optional<EfUser> getFullProfileByUsername(String username);

    /**
     * Fetches a user profile given the GH handle. Aggregates ECA and committer status information.
     * 
     * @param handle The given user's GH handle
     * @return An Optional containing an EfUser if it exists.
     */
    Optional<EfUser> getFullProfileByHandle(String handle);

    /**
     * Fetches a user profile given the email. Aggregates ECA and committer status information.
     * 
     * @param email The given user's email
     * @return An Optional containing an EfUser if it exists.
     */
    Optional<EfUser> getFullProfileByEmail(String email);

    /**
     * Fetches a user profile given the uid. Aggregates ECA and committer status information.
     * 
     * @param email The given user's email
     * @return An Optional containing an EfUser if it exists.
     */
    Optional<EfUser> getFullProfileByUid(int uid);

    /**
     * Fetches a user profile given the desired search params. Aggregates ECA and committer status information.
     * 
     * @param params The given user's mail, name, and uid fields.
     * @return An Optional conatining an EfUser if it exists.
     */
    Optional<EfUser> getFullProfileByParams(ProfileAPISearchParams params);

    /**
     * Fetches a user's mailing-list subscriptions by querying the Mailing-list-API using the given username. The
     * "mailing_list_subscriptions" field may be empty if the user does not exist or has no subscriptions.
     * 
     * @param username The given ef username
     * @return A subscriptions entity.
     */
    Subscriptions getSubscriptionsByUsername(String username);

    /**
     * Fetches a list of documents signed by the user or user's org to check if they are covered by the ECA. Returns the user ECA status.
     * Will return false for both fields if the user does not exist.
     * 
     * @param username The given ef username used for filtering.
     * @return An ECA object with user eca status.
     */
    Eca getEcaStatus(String username);

    /**
     * Returns a map of a person's associated projects, grouped by project id
     * 
     * @param username The given ef username
     * @return A map of the given person's associated projects
     */
    MultivaluedMap<String, PeopleProject> getPersonProjects(String username);

    /**
     * Returns a GerritResponse object with the user's review count and additional account URLs.
     * 
     * @param username The desired user's username.
     * @return An Optional containing the user gerrit count if they exist
     */
    Optional<GerritResponse> getUserGerritCount(String username, int uid, String email);
}
