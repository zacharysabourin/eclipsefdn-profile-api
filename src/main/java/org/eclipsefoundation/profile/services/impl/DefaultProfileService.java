/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.services.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.SplittableRandom;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.context.ManagedExecutor;
import org.eclipsefoundation.core.exception.ApplicationException;
import org.eclipsefoundation.core.helper.LoggingHelper;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.api.models.EfUser.Eca;
import org.eclipsefoundation.efservices.api.models.EfUser.PublisherAgreement;
import org.eclipsefoundation.efservices.api.models.Project;
import org.eclipsefoundation.efservices.services.ProjectService;
import org.eclipsefoundation.foundationdb.client.model.OrganizationContactData;
import org.eclipsefoundation.foundationdb.client.model.full.FullOrganizationContactData;
import org.eclipsefoundation.foundationdb.client.model.full.FullPeopleProjectData;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.eclipsefoundation.profile.api.models.AccountsProfileData;
import org.eclipsefoundation.profile.config.UserMetadataConfig;
import org.eclipsefoundation.profile.dtos.eclipseapi.GerritCount;
import org.eclipsefoundation.profile.helpers.ProfileHelper;
import org.eclipsefoundation.profile.helpers.UserMetadataHelper;
import org.eclipsefoundation.profile.models.GerritResponse;
import org.eclipsefoundation.profile.models.LdapResult;
import org.eclipsefoundation.profile.models.PeopleProject;
import org.eclipsefoundation.profile.models.ProfileAPISearchParams;
import org.eclipsefoundation.profile.models.Subscriptions;
import org.eclipsefoundation.profile.namespace.ProfileAPIParameterNames;
import org.eclipsefoundation.profile.services.LDAPService;
import org.eclipsefoundation.profile.services.ProfileService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.runtime.Startup;

/**
 * Default implementation for the ProfileService. Uses the Ef CachingService to cache all external data fetches.
 */
@Startup
@ApplicationScoped
public class DefaultProfileService implements ProfileService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultProfileService.class);

    private static final SplittableRandom rand = new SplittableRandom();
    private static final int RANDOM_BOUND = 100;
    private static final int RANDOM_PROBABILITY = 1;
    private static final long FUTURE_TIMEOUT = 5;

    @Inject
    UserMetadataConfig metadataConfig;

    @Inject
    LDAPService ldapService;
    @Inject
    ProjectService projectService;
    @Inject
    UserMetadataHelper metadataHelper;
    @Inject
    ProfileHelper profileHelper;

    @Inject
    ManagedExecutor executor;

    @Inject
    DefaultHibernateDao defaultDao;
    @Inject
    FilterService filters;
    @Inject
    RequestWrapper wrap;

    @Inject
    CachingService cache;

    @PostConstruct
    void init() {
        // pre-load all spec projects without loading all other entities from project service. Discarding the results
        projectService.getAllSpecProjects();
    }

    @Override
    public Optional<EfUser> getSlimProfileByUsername(String username) {
        try {
            // Search in LDAP and accounts asynchronously
            CompletableFuture<Optional<LdapResult>> ldapSearch = executor.supplyAsync(() -> ldapService.searchLdapByUsername(username));
            CompletableFuture<Optional<AccountsProfileData>> accountsSearch = executor
                    .supplyAsync(() -> profileHelper.getAccountsProfileByUsername(username));

            Optional<LdapResult> ldapResult = ldapSearch.get(FUTURE_TIMEOUT, TimeUnit.SECONDS);
            Optional<AccountsProfileData> accountsResult = accountsSearch.get(FUTURE_TIMEOUT, TimeUnit.SECONDS);

            // Both need to exist to pass to next step
            return ldapResult.isEmpty() || accountsResult.isEmpty() ? Optional.empty()
                    : buildSlimEfUser(ldapResult.get(), accountsResult.get());
        } catch (Exception e) {
            Thread.currentThread().interrupt();
            throw new ApplicationException(String.format("Something broke while fetching profile data for user: %s", username), e,
                    Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
        }
    }

    @Override
    public Optional<EfUser> getFullProfileByUsername(String username) {
        Optional<EfUser> slimUser = getSlimProfileByUsername(username);
        return slimUser.isEmpty() ? slimUser : augmentSlimUser(slimUser);
    }

    @Override
    public Optional<EfUser> getFullProfileByHandle(String handle) {
        // Search in LDAP
        Optional<LdapResult> ldapResult = ldapService.searchLdapByGhHandle(handle);
        if (ldapResult.isEmpty()) {
            return Optional.empty();
        }

        // Search using accounts if they exist in LDAP
        Optional<AccountsProfileData> accountsResult = profileHelper.getAccountsProfileByUsername(ldapResult.get().getUsername());
        return accountsResult.isEmpty() ? Optional.empty() : augmentSlimUser(buildSlimEfUser(ldapResult.get(), accountsResult.get()));
    }

    @Override
    public Optional<EfUser> getFullProfileByEmail(String email) {
        // Search for user in LDAP
        Optional<LdapResult> ldapResult = ldapService.searchLdapByEmail(email);
        if (ldapResult.isEmpty()) {
            return Optional.empty();
        }

        // Search using accounts if they exist in LDAP
        Optional<AccountsProfileData> accountsResult = profileHelper.getAccountsProfileByUsername(ldapResult.get().getUsername());
        return accountsResult.isEmpty() ? Optional.empty() : augmentSlimUser(buildSlimEfUser(ldapResult.get(), accountsResult.get()));
    }

    @Override
    public Optional<EfUser> getFullProfileByUid(int uid) {
        // Search using accounts
        Optional<AccountsProfileData> accountsResult = profileHelper.getAccountsProfileByUid(uid);
        if (accountsResult.isEmpty()) {
            return Optional.empty();
        }

        // Search for user in LDAP if they exist in accounts
        Optional<LdapResult> ldapResult = ldapService.searchLdapByUsername(accountsResult.get().getName());
        return ldapResult.isEmpty() ? Optional.empty() : augmentSlimUser(buildSlimEfUser(ldapResult.get(), accountsResult.get()));
    }

    @Override
    public Optional<EfUser> getFullProfileByParams(ProfileAPISearchParams params) {

        Optional<EfUser> user = Optional.empty();

        // Load by UID if it's present
        Integer uid = params.getUid().orElse(null);
        if (uid != null) {
            user = getFullProfileByUid(uid);
        }

        // Load by username if unable to load via uid
        String name = params.getName().orElse("");
        if (StringUtils.isNotBlank(name) && user.isEmpty()) {
            user = getFullProfileByUsername(name);
        }

        // Load by mail if unable to load via username or uid
        String mail = params.getMail().orElse("");
        if (StringUtils.isNotBlank(mail) && user.isEmpty()) {
            user = getFullProfileByEmail(mail);
        }

        return user;
    }

    @Override
    public Eca getEcaStatus(String username) {

        // Check for personal ECA first
        boolean ecaStatus = metadataHelper.userCoveredByEca(profileHelper.getPeopleDocumentsByUsername(username));
        if (!ecaStatus) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("No personal ECA for user: {}", LoggingHelper.format(username));
            }

            // Fetch user's org data and use the org id for ECA validation if they are an org contact
            Optional<OrganizationContactData> orgContactData = profileHelper.getOrgContactByUsername(username);
            if (orgContactData.isPresent()) {
                String orgId = Integer.toString(orgContactData.get().getOrganizationID());
                ecaStatus = metadataHelper.userOrgCoveredByEca(profileHelper.getDocumentsByOrg(orgId));
            }
        }

        return Eca.builder().setSigned(ecaStatus).setCanContributeSpecProject(ecaStatus).build();
    }

    @Override
    public Subscriptions getSubscriptionsByUsername(String username) {
        return Subscriptions.builder().setMailingListSubscriptions(profileHelper.getMailingListsByUsername(username)).build();
    }

    @Override
    public MultivaluedMap<String, PeopleProject> getPersonProjects(String username) {
        try {
            // Load all PeopleProjects and spec projects asynchronously
            CompletableFuture<List<FullPeopleProjectData>> projectsSearch = executor
                    .supplyAsync(() -> profileHelper.getFullPeopleProjectsByUsername(username));
            CompletableFuture<List<Project>> specProjectSearch = executor.supplyAsync(() -> projectService.getAllSpecProjects());

            List<FullPeopleProjectData> projectsResults = projectsSearch.get(FUTURE_TIMEOUT, TimeUnit.SECONDS);
            List<Project> specProjectsResult = specProjectSearch.get(FUTURE_TIMEOUT, TimeUnit.SECONDS);

            // Map each project to it's projectID, check through specProjects if id matches
            Optional<MultivaluedMap<String, PeopleProject>> results = cache
                    .get(username, new MultivaluedMapImpl<>(), MultivaluedMap.class,
                            () -> projectsResults.stream().collect(MultivaluedMapImpl::new, (multimap, project) -> {
                                String id = project.getProject().getProjectID();
                                boolean isSpecProject = specProjectsResult.stream().anyMatch(s -> s.getProjectId().equalsIgnoreCase(id));
                                multimap.add(id, metadataHelper.buildPeopleProject(project, isSpecProject));
                            }, MultivaluedMapImpl::putAll));

            return results.isPresent() ? results.get() : new MultivaluedMapImpl<>();
        } catch (Exception e) {
            Thread.currentThread().interrupt();
            throw new ApplicationException(String.format("Something broke while fetching project data for user: %s", username), e,
                    Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
        }
    }

    @Override
    public Optional<GerritResponse> getUserGerritCount(String username, int uid, String email) {
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(ProfileAPIParameterNames.UID.getName(), Integer.toString(uid));

        int count = 0;
        List<GerritCount> results = defaultDao.get(new RDBMSQuery<>(wrap, filters.get(GerritCount.class), params));
        if (results != null && !results.isEmpty()) {

            // If initial record exists, set the starting count
            count = results.get(0).getReviewCount();

            // Return current results if not older than 24hrs
            if (!metadataHelper.isExpiredReport(results.get(0).getReportDate())) {
                return cache
                        .get(username, new MultivaluedMapImpl<>(), GerritResponse.class,
                                () -> metadataHelper.buildGerritResponse(results.get(0).getReviewCount(), email, username));
            }
        }

        // 1% chance to fully recount
        if (rand.split().nextInt(RANDOM_BOUND) == RANDOM_PROBABILITY) {
            count = 0;
        }

        // Get all gerrit changes and add the count
        count += profileHelper.getGerritChanges(email, count).size();

        // Add updated/new record to DB
        defaultDao
                .add(new RDBMSQuery<>(wrap, filters.get(GerritCount.class)), Arrays.asList(metadataHelper.buildGerritCountDto(uid, count)));

        final int finalCount = count;
        return cache
                .get(username, new MultivaluedMapImpl<>(), GerritResponse.class,
                        () -> metadataHelper.buildGerritResponse(finalCount, email, username));
    }

    /**
     * Builds a slim version of an EfUser using the given LDAP and accounts information. A slimmed down EfUser does not contain the
     * relationship URLs, eca/committer status. Fetches from the accounts api for the remaining profile data.
     * 
     * @param ldapResult The fetched LdapResult entity.
     * @param accountsResult The fetched EfUser entity form accounts.
     * @return A EfUser populated with basic profile data.
     */
    private Optional<EfUser> buildSlimEfUser(LdapResult ldapResult, AccountsProfileData accountsResult) {
        LOGGER.debug("Building slim EFUser with name: '{}'", ldapResult.getUsername());
        return Optional
                .of(EfUser
                        .builder()
                        .setUid(accountsResult.getUid())
                        .setName(ldapResult.getUsername())
                        .setPicture(accountsResult.getPicture())
                        .setMail(ldapResult.getMail())
                        .setFirstName(ldapResult.getFirstName())
                        .setLastName(ldapResult.getLastName())
                        .setFullName(ldapResult.getFullName())
                        .setPublisherAgreements(Collections.emptyMap())
                        .setGithubHandle(ldapResult.getGithubId())
                        .setTwitterHandle(accountsResult.getTwitterHandle())
                        .setJobTitle(accountsResult.getJobTitle())
                        .setWebsite(accountsResult.getWebsite())
                        .setCountry(accountsResult.getCountry())
                        .setBio(accountsResult.getBio())
                        .setInterests(accountsResult.getInterests())
                        .setOrg(accountsResult.getOrg())
                        .build());
    }

    /**
     * Builds a full EFUser entity using the given slim version. Fetches from fdndb-api for additional ECA/committer status
     * 
     * @param slimUser The slimmed down EfUser.
     * @return A populated EfUser entity with all relevant data.
     */
    private Optional<EfUser> augmentSlimUser(Optional<EfUser> slimUser) {
        if (slimUser.isEmpty()) {
            return slimUser;
        }

        EfUser.Builder slimUserBuilder = slimUser.get().toBuilder();
        String username = slimUser.get().getName();
        LOGGER.debug("Building full EFUser with name: '{}'", username);

        try {
            // Load all OrgContactData, eca data, committer status, and PA data asynchronously
            CompletableFuture<Map<String, PublisherAgreement>> agreementSearch = executor
                    .supplyAsync(() -> metadataHelper.getAgreementStatus(profileHelper.getPeopleDocumentsByUsername(username)));
            CompletableFuture<Optional<FullOrganizationContactData>> orgContactSearch = executor
                    .supplyAsync(() -> profileHelper.getFullOrgContactByUsername(username));
            CompletableFuture<Boolean> committerStatusSearch = executor.supplyAsync(() -> getCommitterStatus(username));
            CompletableFuture<Eca> ecaSearch = executor.supplyAsync(() -> getEcaStatus(username));

            Optional<FullOrganizationContactData> orgContactData = orgContactSearch.get(FUTURE_TIMEOUT, TimeUnit.SECONDS);
            if (!orgContactData.isEmpty() && orgContactData.get().getSysRelation().getRelation().equalsIgnoreCase("EMPLY")) {
                slimUserBuilder
                        .setOrg(orgContactData.get().getOrganization().getName1())
                        .setOrgId(Integer.toString(orgContactData.get().getOrganization().getOrganizationID()));
            }

            return Optional
                    .of(slimUserBuilder
                            .setEcaUrl(metadataHelper.buildRelationshipUrl(username, "eca"))
                            .setProjectsUrl(metadataHelper.buildRelationshipUrl(username, "projects"))
                            .setGerritUrl(metadataHelper.buildRelationshipUrl(username, "gerrit"))
                            .setMailinglistUrl(metadataHelper.buildRelationshipUrl(username, "mailing-list"))
                            .setMpcFavoritesUrl(metadataConfig.url().marketplaceFavoritesUrl() + username)
                            .setEca(ecaSearch.get(FUTURE_TIMEOUT, TimeUnit.SECONDS))
                            .setIsCommitter(committerStatusSearch.get(FUTURE_TIMEOUT, TimeUnit.SECONDS))
                            .setPublisherAgreements(agreementSearch.get(FUTURE_TIMEOUT, TimeUnit.SECONDS))
                            .build());
        } catch (Exception e) {
            Thread.currentThread().interrupt();
            throw new ApplicationException(String.format("Something broke while fetching metadata for user: %s", username), e,
                    Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
        }
    }

    /**
     * Fetches a list of projects for a given user. Returns true if the user has a committer status on any project.
     * 
     * @param username The given ef username used for filtering
     * @return True if user has committer status. False if not.
     */
    private boolean getCommitterStatus(String username) {
        // If none match committer status, user not committer
        return profileHelper
                .getFullPeopleProjectsByUsername(username)
                .stream()
                .anyMatch(p -> p.getRelation().getRelation().equalsIgnoreCase("cm"));
    }
}
