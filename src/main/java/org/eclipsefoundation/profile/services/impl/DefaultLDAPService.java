/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.services.impl;

import java.util.List;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.core.helper.LoggingHelper;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.profile.config.LDAPConnectionConfig;
import org.eclipsefoundation.profile.models.LdapResult;
import org.eclipsefoundation.profile.namespace.LdapFieldNames;
import org.eclipsefoundation.profile.namespace.ProfileAPIParameterNames;
import org.eclipsefoundation.profile.services.LDAPConnectionWrapper;
import org.eclipsefoundation.profile.services.LDAPService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unboundid.ldap.sdk.DN;
import com.unboundid.ldap.sdk.Filter;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.SearchRequest;
import com.unboundid.ldap.sdk.SearchResultEntry;
import com.unboundid.ldap.sdk.SearchScope;

/**
 * Default LDAPService implementation. Uses the EF CachigService to cache any LDAP search results.
 */
@ApplicationScoped
public class DefaultLDAPService implements LDAPService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultLDAPService.class);

    @Inject
    LDAPConnectionConfig config;

    @Inject
    CachingService cache;
    @Inject
    LDAPConnectionWrapper ldapWrap;

    @Override
    public Optional<LdapResult> searchLdapByUsername(String efUsername) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Searching LDAP for user: {}", LoggingHelper.format(efUsername));
        }

        Optional<LdapResult> result = cache
                .get(efUsername, buildCacheParam("user"), LdapResult.class,
                        () -> searchLdap(Filter.createEqualityFilter(ProfileAPIParameterNames.UID.getName(), efUsername)));
        if (result.isEmpty() && LOGGER.isWarnEnabled()) {
            LOGGER.warn("LDAP - no user with name: {}", LoggingHelper.format(efUsername));
        }
        return result;
    }

    @Override
    public Optional<LdapResult> searchLdapByGhHandle(String ghHandle) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Searching LDAP for user with GH handle: {}", LoggingHelper.format(ghHandle));
        }

        Optional<LdapResult> result = cache
                .get(ghHandle, buildCacheParam("handle"), LdapResult.class,
                        () -> searchLdap(Filter.createEqualityFilter(LdapFieldNames.EMPLOYEE_TYPE, "GITHUB:" + ghHandle)));
        if (result.isEmpty() && LOGGER.isWarnEnabled()) {
            LOGGER.warn("LDAP - no user with GH id: {}", LoggingHelper.format(ghHandle));
        }

        return result;
    }

    @Override
    public Optional<LdapResult> searchLdapByEmail(String email) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Searching LDAP for user with email: {}", LoggingHelper.format(email));
        }

        Optional<LdapResult> result = cache
                .get(email, new MultivaluedMapImpl<>(), LdapResult.class,
                        () -> searchLdap(Filter.createEqualityFilter(ProfileAPIParameterNames.MAIL.getName(), email)));
        if (result.isEmpty() && LOGGER.isWarnEnabled()) {
            LOGGER.warn("LDAP - no user with email: {}", LoggingHelper.format(email));
        }
        return result;
    }

    /**
     * Performs an LDAP search using the desired filter. Creates a request using the given Filter, performs a search, and constructs an
     * LdapResult entity from the entry if found. Returns an empty LdapResult object if no results were found or if there was a connection
     * error.
     *
     * @param searchFilter The search filter used in the search
     * @return the LdapResult or null
     */
    private LdapResult searchLdap(Filter searchFilter) {
        try {
            // Create a search request with base dn, scope, and filter
            DN dn = new DN(config.baseDn());
            SearchRequest request = new SearchRequest(dn, SearchScope.SUB, searchFilter);
            LOGGER.debug("LDAP REQUEST: {}", request);

            // Perform search and get entries
            List<SearchResultEntry> searchEntries = ldapWrap.connectAndSearch(request);
            if (searchEntries == null || searchEntries.isEmpty()) {
                LOGGER.error("No results found using filter: {}", searchFilter);
                return null;
            }

            // Only using equality filters. 1 result expected
            SearchResultEntry entry = searchEntries.get(0);
            LOGGER.debug("LDAP RESULT: {}", entry);

            return LdapResult
                    .builder()
                    .setUsername(entry.getAttributeValue(ProfileAPIParameterNames.UID.getName()))
                    .setMail(entry.getAttributeValue(ProfileAPIParameterNames.MAIL.getName()))
                    .setGithubId(isolateGhHandle(entry.getAttributeValue(LdapFieldNames.EMPLOYEE_TYPE)))
                    .setFirstName(entry.getAttributeValue(LdapFieldNames.SN))
                    .setLastName(entry.getAttributeValue(LdapFieldNames.GIVEN_NAME))
                    .setFullName(entry.getAttributeValue(LdapFieldNames.CN))
                    .build();

        } catch (LDAPException e) {
            LOGGER.error("Error constructing DN from config", e);
            return null;
        }
    }

    /**
     * Strips the "GITHUB:" part of the employeeType attribute, isolating the handle.
     * 
     * @param employeeType The employeetype attribute
     * @return The isolated GH id.
     */
    private String isolateGhHandle(String employeeType) {
        return employeeType == null ? employeeType : employeeType.replace("GITHUB:", "");
    }

    /**
     * Builds a MultivaluedMap containing the desired cache param. This is to eliminate collision between a username and handle that are the
     * same but belong to different people.
     * 
     * @param strategy The caching strategy. Ex: user | handle
     * @return A MultivaluedMap containing the desired cache strategy.
     */
    private MultivaluedMap<String, String> buildCacheParam(String strategy) {
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(ProfileAPIParameterNames.STRATEGY.getName(), strategy);
        return params;
    }
}
