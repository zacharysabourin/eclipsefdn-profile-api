/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.services.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.MultivaluedMap;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.helper.DateTimeHelper;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.foundationdb.client.model.SysModLogData;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.eclipsefoundation.profile.api.SysAPI;
import org.eclipsefoundation.profile.daos.EclipsePersistenceDao;
import org.eclipsefoundation.profile.dtos.eclipse.AccountRequests;
import org.eclipsefoundation.profile.dtos.eclipseapi.UserDeleteRequest;
import org.eclipsefoundation.profile.helpers.UserDeleteHelper;
import org.eclipsefoundation.profile.models.DeleteRequestData;
import org.eclipsefoundation.profile.models.ProfileAPISearchParams;
import org.eclipsefoundation.profile.models.mappers.DeleteRequestMapper;
import org.eclipsefoundation.profile.services.UserDeleteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default implementation of the UserDeleteService. Handles all UserDeleteRequest CRUD operations. Makes use of the UserDeleteHelper for
 * certain processes.
 */
@ApplicationScoped
public class DefaultUserDeleteService implements UserDeleteService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultUserDeleteService.class);

    // List of hosts used for UserDeleteRequest creation
    @ConfigProperty(name = "eclipse.profile.user-delete.hosts")
    Map<String, String> hosts;

    @Inject
    UserDeleteHelper userDeleteHelper;

    @RestClient
    SysAPI sysAPI;

    @Inject
    DefaultHibernateDao defaultDao;
    @Inject
    EclipsePersistenceDao eclipseDao;
    @Inject
    FilterService filters;
    @Inject
    RequestWrapper wrap;

    @Inject
    DeleteRequestMapper mapper;

    @Override
    public List<DeleteRequestData> getDeleteRequests(ProfileAPISearchParams params) {
        return fetchDeleteRequests(userDeleteHelper.mapQueryToDBParams(params));
    }

    @Override
    public Optional<DeleteRequestData> getRequestById(int id) {
        List<DeleteRequestData> results = fetchDeleteRequests(userDeleteHelper.buildIdParams(id));
        return results.isEmpty() ? Optional.empty() : Optional.of(results.get(0));
    }

    @Override
    public Optional<UserDeleteRequest> updateDeleteRequest(DeleteRequestData src) {
        LOGGER.debug("Updating UserDeleteRequest: {}", src);
        List<UserDeleteRequest> added = persistDeleteRequests(Arrays.asList(mapper.toDTO(src, defaultDao)));
        return added.isEmpty() ? Optional.empty() : Optional.of(added.get(0));

    }

    @Override
    public void deleteDeleteRequest(int id) {
        try {
            LOGGER.debug("Deleting UserDeleteRequest with id: {}", id);
            defaultDao.delete(new RDBMSQuery<>(wrap, filters.get(UserDeleteRequest.class), userDeleteHelper.buildIdParams(id)));
        } catch (Exception e) {
            LOGGER.error("Error deleting delete requests with id: {}", id, e);
        }
    }

    @Override
    public List<DeleteRequestData> createDeleteRequestsForUser(EfUser user) {

        int now = (int) TimeUnit.MILLISECONDS.toSeconds(DateTimeHelper.getMillis());

        // Create a new request entity for each host
        List<UserDeleteRequest> toAdd = hosts.values().stream().map(host -> {
            UserDeleteRequest request = new UserDeleteRequest();
            request.setUid(user.getUid());
            request.setName(user.getName());
            request.setMail(user.getMail());
            request.setHost(host);
            request.setStatus(0);
            request.setCreated(now);
            request.setChanged(now);
            return request;
        }).collect(Collectors.toList());

        // Persist all entities in DB
        return persistDeleteRequests(toAdd).stream().map(mapper::toModel).collect(Collectors.toList());
    }

    @Override
    public Optional<AccountRequests> persistAccountRequestWithModLog(EfUser user) {
        try {
            // Persist a deletion AccountsRequest
            List<AccountRequests> added = eclipseDao
                    .add(new RDBMSQuery<>(wrap, filters.get(AccountRequests.class)),
                            Arrays.asList(userDeleteHelper.buildDeletionAccountRequest(user.getMail(), user.getName())));

            // Track system event
            sysAPI
                    .insertSysModLog(SysModLogData
                            .builder()
                            .setLogTable("ProfileAPI")
                            .setPK1(user.getMail())
                            .setPK2(Integer.toString(user.getUid()))
                            .setLogAction("DELETEACCOUNT")
                            .setPersonId(user.getName())
                            .setModDateTime(DateTimeHelper.now())
                            .build());

            return added.isEmpty() ? Optional.empty() : Optional.of(added.get(0));
        } catch (Exception e) {
            LOGGER.error("Something went wrong while persisting AccountRequest for user: {}", user.getName(), e);
            sysAPI
                    .insertSysModLog(SysModLogData
                            .builder()
                            .setLogTable("ProfileAPI")
                            .setPK1("persistAccountRequestWithModLog")
                            .setPK2(userDeleteHelper.getBestMatchingIP())
                            .setLogAction("sql_error")
                            .setPersonId(user.getMail())
                            .setModDateTime(DateTimeHelper.now())
                            .build());
            return Optional.empty();
        }
    }

    /**
     * Persists a list of UserDeleteRequest entities into the DB.
     * 
     * @param src The given UserDeleteRequest entities
     * @return The list of persisted entities if they exist
     */
    private List<UserDeleteRequest> persistDeleteRequests(List<UserDeleteRequest> src) {
        try {
            return defaultDao.add(new RDBMSQuery<>(wrap, filters.get(UserDeleteRequest.class)), src);
        } catch (Exception e) {
            LOGGER.error("Error persisting delete requests: {}", src, e);
            return Collections.emptyList();
        }
    }

    /**
     * Fetches all UserDeleteRequests from the eclipse_api DB using the given params and returns all results mapped to the DeleteRequestData
     * class.
     * 
     * @param params The desired search params.
     * @return A List of UserDeleteRequest entities if they exist
     */
    private List<DeleteRequestData> fetchDeleteRequests(MultivaluedMap<String, String> params) {
        try {
            LOGGER.debug("Fetching all UserDeleteRequests with params: {}", params);
            List<UserDeleteRequest> results = defaultDao.get(new RDBMSQuery<>(wrap, filters.get(UserDeleteRequest.class), params));
            LOGGER.debug("Found {} result(s)", results.size());
            return results.stream().map(mapper::toModel).collect(Collectors.toList());
        } catch (Exception e) {
            LOGGER.error("Error fetching delete requests with params: {}", params, e);
            return Collections.emptyList();
        }
    }
}
