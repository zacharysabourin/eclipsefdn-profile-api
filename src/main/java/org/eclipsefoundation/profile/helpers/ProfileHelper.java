/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.helpers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.MultivaluedMap;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.helper.LoggingHelper;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.efservices.services.DrupalTokenService;
import org.eclipsefoundation.foundationdb.client.model.OrganizationContactData;
import org.eclipsefoundation.foundationdb.client.model.OrganizationDocumentData;
import org.eclipsefoundation.foundationdb.client.model.PeopleDocumentData;
import org.eclipsefoundation.foundationdb.client.model.full.FullOrganizationContactData;
import org.eclipsefoundation.foundationdb.client.model.full.FullPeopleProjectData;
import org.eclipsefoundation.profile.api.DrupalAccountsAPI;
import org.eclipsefoundation.profile.api.GerritAPI;
import org.eclipsefoundation.profile.api.MailingListAPI;
import org.eclipsefoundation.profile.api.OrganizationAPI;
import org.eclipsefoundation.profile.api.PeopleAPI;
import org.eclipsefoundation.profile.api.models.AccountsProfileData;
import org.eclipsefoundation.profile.api.models.GerritChangeData;
import org.eclipsefoundation.profile.api.models.MailingListData;
import org.eclipsefoundation.profile.namespace.ProfileAPIParameterNames;
import org.eclipsefoundation.profile.services.ProfileService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Helper class used to fetch data for the {@link ProfileService} from various APIs. Caches all results.
 */
@ApplicationScoped
public class ProfileHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProfileHelper.class);

    private static final int GERRIT_RESPONSE_START_INDEX = 4;

    @RestClient
    PeopleAPI peopleAPI;
    @RestClient
    OrganizationAPI orgAPI;
    @RestClient
    MailingListAPI mailingListAPI;
    @RestClient
    GerritAPI gerritAPI;
    @RestClient
    DrupalAccountsAPI accountsAPI;

    @Inject
    DrupalTokenService tokenService;

    @Inject
    CachingService cache;

    @Inject
    ObjectMapper objectMapper;

    /**
     * Queries the accounts-api for profile data for the given user via username.
     * 
     * @param username The given username
     * @return An Optional containing the AccountsProfileData entity if it exists, or empty.
     */
    public Optional<AccountsProfileData> getAccountsProfileByUsername(String username) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching Accounts profile data for user: {}", LoggingHelper.format(username));
        }

        Optional<AccountsProfileData> results = cache
                .get(username, new MultivaluedMapImpl<>(), AccountsProfileData.class,
                        () -> accountsAPI.getAccountsProfileByUsername(username));
        if (results.isEmpty() && LOGGER.isWarnEnabled()) {
            LOGGER.warn("Error - accounts-api: No accounts results for user: {}", LoggingHelper.format(username));
        }

        return results;
    }

    /**
     * Queries the accounts-api for profile data for the given user via drupal uid.
     * 
     * @param uid The given uid
     * @return An Optional containing the AccountsProfileData entity if it exists, or empty.
     */
    public Optional<AccountsProfileData> getAccountsProfileByUid(int uid) {

        LOGGER.debug("Fetching Accounts profile data for user with uid: {}", uid);

        Optional<List<AccountsProfileData>> results = cache
                .get(Integer.toString(uid), new MultivaluedMapImpl<>(), AccountsProfileData.class,
                        () -> accountsAPI.getAccountsProfileByUid(uid, getBearerToken()));
        if (results.isEmpty() || results.get().isEmpty()) {
            LOGGER.warn("Error - accounts-api: No accounts results for user with uid: {}", uid);
            return Optional.empty();
        }

        return Optional.of(results.get().get(0));
    }

    /**
     * Queries Fdndb-api for any FullPeopleProject data for the given user.
     * 
     * @param username The given username
     * @return A List of FullPeopleProjectData or empty
     */
    public List<FullPeopleProjectData> getFullPeopleProjectsByUsername(String username) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching PeopleProjects for user: {}", LoggingHelper.format(username));
        }

        Optional<List<FullPeopleProjectData>> results = cache
                .get(username, new MultivaluedMapImpl<>(), FullPeopleProjectData.class, () -> peopleAPI.getFullPeopleProjects(username));
        if (results.isEmpty()) {
            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn("Error - fdndb-api: No project results for user: {}", LoggingHelper.format(username));
            }

            return Collections.emptyList();
        }

        return results.get();
    }

    /**
     * Queries Fdndb-api for any OrganizationContactData data for the given user.
     * 
     * @param username The given username
     * @return A List of OrganizationContactData or empty
     */
    public Optional<OrganizationContactData> getOrgContactByUsername(String username) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching OrganizationContactData for user: {}", LoggingHelper.format(username));
        }

        Optional<List<OrganizationContactData>> results = cache
                .get(username, new MultivaluedMapImpl<>(), OrganizationContactData.class, () -> orgAPI.getOrgContacts(username));
        if (results.isEmpty() || results.get().isEmpty()) {
            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn("Error - fdndb-api: No org contact results for user: {}", LoggingHelper.format(username));
            }

            return Optional.empty();
        }

        return Optional.of(results.get().get(0));
    }

    /**
     * Queries Fdndb-api for any FullOrganizationContactData data for the given user.
     * 
     * @param username The given username
     * @return A List of FullOrganizationContactData or empty
     */
    public Optional<FullOrganizationContactData> getFullOrgContactByUsername(String username) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching FullOrganizationContactData for user: {}", LoggingHelper.format(username));
        }

        Optional<List<FullOrganizationContactData>> results = cache
                .get(username, new MultivaluedMapImpl<>(), FullOrganizationContactData.class, () -> orgAPI.getFullOrgContacts(username));
        if (results.isEmpty() || results.get().isEmpty()) {
            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn("Error - fdndb-api: No org contact results for user: {}", LoggingHelper.format(username));
            }

            return Optional.empty();
        }

        return Optional.of(results.get().get(0));
    }

    /**
     * Queries Fdndb-api for any document records signed by the given user.
     * 
     * @param username The given username
     * @return A List of PeopleDocumentData or empty
     */
    public List<PeopleDocumentData> getPeopleDocumentsByUsername(String username) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching PeopleDocuments for user: {}", LoggingHelper.format(username));
        }

        Optional<List<PeopleDocumentData>> results = cache
                .get(username, new MultivaluedMapImpl<>(), PeopleDocumentData.class, () -> peopleAPI.getDocuments(username));
        if (results.isEmpty()) {
            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn("Fdndb-api: No PeopleDocument results for user: {}", LoggingHelper.format(username));
            }

            return Collections.emptyList();
        }

        return results.get();
    }

    /**
     * Queries Fdndb-api for any document records signed by the given org.
     * 
     * @param orgId The given orgId
     * @return A List of OrganizationDocumentData or empty
     */
    public List<OrganizationDocumentData> getDocumentsByOrg(String orgId) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching OrganizationDocuments for org: {}", LoggingHelper.format(orgId));
        }

        Optional<List<OrganizationDocumentData>> results = cache
                .get(orgId, new MultivaluedMapImpl<>(), OrganizationDocumentData.class, () -> orgAPI.getOrganizationDocuments(orgId));
        if (results.isEmpty()) {
            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn("Fdndb-api: No OrganizationDocument results for org: {}", LoggingHelper.format(orgId));
            }

            return Collections.emptyList();
        }

        return results.get();
    }

    /**
     * Queries the Mailing-list-api for a list of mailing-list objects for a given user.
     * 
     * @param username The given username
     * @return A List of MailingListData objects or empty
     */
    public List<MailingListData> getMailingListsByUsername(String username) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching mailing-list-api for user: {}", LoggingHelper.format(username));
        }

        Optional<List<MailingListData>> results = cache
                .get(username, new MultivaluedMapImpl<>(), MailingListData.class, () -> mailingListAPI.getMailingListsByUsername(username));
        if (results.isEmpty()) {
            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn("Mailing-list-API: No results for user: {}", LoggingHelper.format(username));
            }

            return Collections.emptyList();
        }

        return results.get();
    }

    /**
     * Queries the Gerrit API for any changes made by a given user ignoring the first 'x' changes, where x is the current number of tracked
     * changes.
     * 
     * @param email The given user's email
     * @param start The starting cursor position
     * @return A List of GerritChangeData or empty
     */
    public List<GerritChangeData> getGerritChanges(String email, int start) {
        LOGGER.debug("Fetching gerrit changes for user with email: {}", email);

        MultivaluedMap<String, String> cacheParams = new MultivaluedMapImpl<>();
        cacheParams.add(ProfileAPIParameterNames.CURSOR.getName(), Integer.toString(start));

        Optional<List<GerritChangeData>> results = cache
                .get(email, cacheParams, GerritChangeData.class, () -> getAllGerritChanges(email, start));
        if (results.isEmpty() || results.get().isEmpty()) {
            LOGGER.warn("Gerrit: No new changes beyond {} for {}", start, email);

            return Collections.emptyList();
        }

        return results.get();
    }

    /**
     * Fetches all Gerrit changes starting at previous count. Checks for the '_more_changes' field in the last record. If this field is set,
     * it will fetch the following page of results. It will fetch until there are no new changes.
     * 
     * @param query The search query
     * @param start The starting cursor position for fetching results.
     * @return A list of all the newest changes since the last query.
     */
    private List<GerritChangeData> getAllGerritChanges(String email, int start) {

        String query = "reviewer:" + email + " status:merged";

        List<GerritChangeData> out = new ArrayList<>();

        // Loop through all results until we hit the end flag
        while (true) {
            List<GerritChangeData> results = convertGerritJson(gerritAPI.getGerritChanges(query, start));
            out.addAll(results);

            // If '_more_changes' field exists in last record and is true, there is more
            if (results.isEmpty() || results.get(results.size() - 1).getMoreChanges() == null
                    || Boolean.FALSE.equals(results.get(results.size() - 1).getMoreChanges())) {
                break;
            }

            // increase the start point before getting next set
            start += results.size();
        }

        return out;
    }

    /**
     * Gerrit adds a special set of characters at the start of their response body. To be able to process the json properly, it must first
     * be removed. ex: )]}' [ ... valid JSON ... ]
     * 
     * @param body The Gerrit json body
     * @return A converted list of GerritchangeData entities if they exist
     */
    private List<GerritChangeData> convertGerritJson(String body) {
        try {
            // Parse the json body after the first 4 characters
            return objectMapper.readerForListOf(GerritChangeData.class).readValue(body.substring(GERRIT_RESPONSE_START_INDEX));
        } catch (Exception e) {
            LOGGER.error("Error parsing json Gerrit data", e);
            return Collections.emptyList();
        }
    }

    /**
     * Builds a bearer token using the token generated by the Oauth server. Will be removed once the accounts-api contains a way to get
     * account data via uid without auithentication.
     * 
     * @return A fully formed Bearer token
     */
    private String getBearerToken() {
        return "Bearer " + tokenService.getToken();
    }
}
