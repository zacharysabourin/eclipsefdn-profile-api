/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.resources;

import java.util.Optional;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipsefoundation.core.helper.LoggingHelper;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.api.models.EfUser.Country;
import org.eclipsefoundation.efservices.models.RequestTokenWrapper;
import org.eclipsefoundation.profile.services.ProfileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Resource class for all profile fetches related to en EF user's Github handle.
 */
@Path("github/profile")
@Produces({ MediaType.APPLICATION_JSON })
public class GithubResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(GithubResource.class);
    private static final String USER_404_MSG_FORMAT = "User '%s' not found";

    @Inject
    ProfileService profileService;

    @Inject
    RequestTokenWrapper requestToken;

    /**
     * Returns a 200 OK Response containing the requested EfUser entity. If the request is unauthenticated, the email and country fields
     * will me anonymized. Returns a 404 Not Found Response if the user cannot be found.
     * 
     * @param handle The given Github handle.
     * @return A 200 OK Response containing the requested EfUser entity. Returns a 404 Not Found Response if the user cannot be found.
     */
    @GET
    @Path("{handle}")
    public Response getUserProfileByHandle(@PathParam("handle") String handle) {
        Optional<EfUser> user = profileService.getFullProfileByHandle(handle);
        if (user.isEmpty()) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Unable to retrieve profile data for GH handle: {}", LoggingHelper.format(handle));
            }

            throw new NotFoundException(String.format(USER_404_MSG_FORMAT, handle));
        }

        // Strip email and anonymize country if request is unauthenticated
        return Response
                .ok(requestToken.isAuthenticated() ? user.get()
                        : user.get().toBuilder().setMail("").setCountry(Country.builder().setCode(null).setName(null).build()).build())
                .build();
    }
}
