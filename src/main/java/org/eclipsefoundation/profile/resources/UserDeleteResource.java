/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.resources;

import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.BeanParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipsefoundation.profile.models.DeleteRequestData;
import org.eclipsefoundation.profile.models.ProfileAPISearchParams;
import org.eclipsefoundation.profile.models.RequestStatusUpdate;
import org.eclipsefoundation.profile.services.UserDeleteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Resource class containing all 'user_delete_request' CRUD endpoints.
 */
@Path("account/user_delete_request")
public class UserDeleteResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserDeleteResource.class);

    private static final String REQUEST_404_MSG_FORMAT = "User delete request with id: '%s' not found";

    private static final int[] VALID_DELETE_STATUSES = { 1, 2 };

    @Inject
    UserDeleteService deleteService;

    /**
     * Returns a 200 OK Response containing all UserDeleteRequest entities that match the given params. Will return an empty list if no
     * matching results are found. All potential ProfileAPISearchParams properties are relevant to this endpoint.
     * 
     * @param params The incoming query params. They include: uid, name, mail, host, status, since, and until
     * @return A 200 OK Response containing all UserDeleteRequest entities that match the given params.
     */
    @GET
    public Response getDeleteRequests(@BeanParam ProfileAPISearchParams params) {
        return Response.ok(deleteService.getDeleteRequests(params)).build();
    }

    /**
     * Returns a 200 OK Response containing the UserDeleteRequestData entity matching the given 'requestId'. Returns a 404 Not Found
     * Response if the requested UserDeleteRequestData entity is not found.
     * 
     * @param requestId The given UserDeleteRequest id.
     * @return A 200 OK Response containing the UserDeleteRequestData entity matching the given 'requestId'. A 404 Not Found Response if the
     * requested entity is not found.
     */
    @GET
    @Path("{requestId}")
    public Response getDeleteRequest(@PathParam("requestId") int requestId) {
        return Response
                .ok(deleteService
                        .getRequestById(requestId)
                        .orElseThrow(() -> new NotFoundException(String.format(REQUEST_404_MSG_FORMAT, requestId))))
                .build();
    }

    /**
     * Returns a 204 No Content if the requested UserDeleteRequest was successfully updated with the status from the request body. Returns a
     * 400 Bad Request Response if the incoming 'status' field is invalid or missing. Returns a 404 Not Found Response if the requested
     * entity cannot be found.
     * 
     * @param requestId The id of the request to update.
     * @param body The incoming body. Should contain single 'status' field.
     * @return A 204 No Content if the requested UserDeleteRequest was successfully updated. A 400 Bad Request Response if the incoming
     * request body's 'status' field is invalid or missing. A 404 Not Found Response if the requested entity cannot be found.
     */
    @PUT
    @Path("{requestId}")
    public Response updateDeleteRequest(@PathParam("requestId") int requestId, @Nullable RequestStatusUpdate body) {

        if (body == null || body.getStatus().isEmpty()) {
            throw new BadRequestException("The 'status' field must be a valid value.");
        }

        // Fetch the request to update
        Optional<DeleteRequestData> toUpdate = deleteService.getRequestById(requestId);
        if (toUpdate.isEmpty()) {
            throw new NotFoundException(String.format(REQUEST_404_MSG_FORMAT, requestId));
        }

        // Can only update status if it is changing to 1 or 2
        int status = body.getStatus().orElse(0);
        if (Arrays.stream(VALID_DELETE_STATUSES).anyMatch(val -> val == status)) {
            DeleteRequestData updated = toUpdate
                    .get()
                    .toBuilder()
                    .setStatus(status)
                    .setChanged((int) TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()))
                    .build();
            deleteService.updateDeleteRequest(updated);
            LOGGER.debug("Updated UserDeleteRequest: {}", toUpdate);
        }

        return Response.status(Status.NO_CONTENT).build();
    }

    /**
     * Returns a 204 Response if the UserDeleteRequest matching the given id was successfully deleted. Returns a 404 Not Found Response if
     * the requested resource was not found.
     * 
     * @param requestId The id of the request to delete.
     * @return A 204 Response if the UserDeleteRequest matching the given id was successfully deleted. A 404 Not Found Response if the
     * requested resource was not found.
     */
    @DELETE
    @Path("{requestId}")
    public Response deleteDeleteRequest(@PathParam("requestId") int requestId) {
        // Request must exist before deletion
        if (deleteService.getRequestById(requestId).isEmpty()) {
            throw new NotFoundException(String.format(REQUEST_404_MSG_FORMAT, requestId));
        }

        deleteService.deleteDeleteRequest(requestId);
        LOGGER.debug("Deleted UserDeleteRequest with id: {}", requestId);

        return Response.status(Status.NO_CONTENT).build();
    }
}
