/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.resources;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipsefoundation.core.exception.ApplicationException;
import org.eclipsefoundation.core.exception.ConflictException;
import org.eclipsefoundation.core.helper.LoggingHelper;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.api.models.EfUser.Country;
import org.eclipsefoundation.efservices.models.RequestTokenWrapper;
import org.eclipsefoundation.efservices.models.RequestUserWrapper;
import org.eclipsefoundation.profile.models.DeleteRequestData;
import org.eclipsefoundation.profile.models.GerritResponse;
import org.eclipsefoundation.profile.models.ProfileAPISearchParams;
import org.eclipsefoundation.profile.services.ProfileService;
import org.eclipsefoundation.profile.services.UserDeleteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Resource class containing all endpoints related to a user's EF profile (not by GH handle). Provides endpoints for user search, user
 * profile, all user metadata, and an endpoint to initiate the user_delete_request process for a user.
 */
@Path("account/profile")
@Produces({ MediaType.APPLICATION_JSON })
public class AccountResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountResource.class);

    private static final String USER_404_MSG_FORMAT = "User '%s' not found";
    private static final String CONFLICT_MSG = "The resource already exists";

    @Inject
    ProfileService profileService;
    @Inject
    UserDeleteService deleteService;

    @Inject
    RequestTokenWrapper requestToken;
    @Inject
    RequestUserWrapper requestUser;

    /**
     * Returns a 200 OK Response containing the user(s) matching the desired params. Returns a 404 Not Found Response if the user(s) cannot
     * be found.
     * 
     * This endpoint is an exception among the profile endpoints. It does not return public data if the incoming request is unauthenticated
     * or has an invalid token. It will instead deny the request. This is because the user email is a protected field and searching via this
     * field should be restricted.
     * 
     * @param params The given search params
     * @return A 200 OK Response containing the user(s) matching the desired params. A 404 Not Found Response if the user(s) cannot be
     * found.
     */
    @GET
    public Response searchForUser(@BeanParam ProfileAPISearchParams params) {

        // If search params are empty, attempt to get current user
        if (params.getMail().isEmpty() && params.getName().isEmpty() && params.getUid().isEmpty()) {

            // Get username from token user data and get user profile
            String username = requestUser.getCurrentUserEfName();
            Optional<EfUser> user = profileService.getFullProfileByUsername(username);

            // If we have a username, this shouldn't happen. But it's possible.
            if (user.isEmpty()) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Unable to retrieve profile data for username: {}", LoggingHelper.format(username));
                }

                throw new NotFoundException(String.format(USER_404_MSG_FORMAT, username));
            }

            return Response.ok(Arrays.asList(user.get())).build();
        }

        // Get user via provided params
        Optional<EfUser> user = profileService.getFullProfileByParams(params);
        if (user.isEmpty()) {
            LOGGER.debug("Unable to retrieve profile data with params: {}", params);
            throw new NotFoundException("User not found");
        }

        return Response.ok(Arrays.asList(user.get())).build();
    }

    /**
     * Returns a 200 OK Response containing the requested EfUser entity. If the request is unauthenticated, the email and country fields
     * will me anonymized. Returns a 404 Not Found Response if the user cannot be found.
     * 
     * @param username The given EF username.
     * @return A 200 OK Response containing the requested EfUser entity. Returns a 404 Not Found Response if the user cannot be found.
     */
    @GET
    @Path("/{username}")
    public Response getUserProfileByUsername(@PathParam("username") String username) {
        Optional<EfUser> user = profileService.getFullProfileByUsername(username);
        if (user.isEmpty()) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Unable to retrieve profile data for username: {}", LoggingHelper.format(username));
            }
            throw new NotFoundException(String.format(USER_404_MSG_FORMAT, username));
        }

        // Strip email and anonymize country if request is unauthenticated
        return Response
                .ok(requestToken.isAuthenticated() ? user.get()
                        : user.get().toBuilder().setMail("").setCountry(Country.builder().setCode(null).setName(null).build()).build())
                .build();
    }

    /**
     * Returns a 200 OK Response containing a given user's ECA status. If the user is not found, an ECA with all false values is returned.
     * 
     * @param username The desired EF user.
     * @return An ECA entity with the desired user's status.
     */
    @GET
    @Path("/{username}/eca")
    public Response getUserEca(@PathParam("username") String username) {
        return Response.ok(profileService.getEcaStatus(username)).build();
    }

    /**
     * Returns a 200 OK Response containing the desired user's Gerrit review count. Returns a 404 Not Found Response if the user cannot be
     * found.
     * 
     * @param username The given username.
     * @return A 200 OK Response containing the desired user's Gerrit review count. A 404 Not Found Response if the user cannot be found.
     */
    @GET
    @Path("/{username}/gerrit")
    public Response getUserGerrit(@PathParam("username") String username) {
        Optional<EfUser> user = profileService.getSlimProfileByUsername(username);
        if (user.isEmpty()) {
            throw new NotFoundException(String.format(USER_404_MSG_FORMAT, username));
        }

        Optional<GerritResponse> info = profileService.getUserGerritCount(username, user.get().getUid(), user.get().getMail());
        if (info.isEmpty()) {
            throw new NotFoundException(String.format(USER_404_MSG_FORMAT, username));
        }

        return Response.ok(info.get()).build();
    }

    /**
     * Returns a 200 OK Response containing the desired user's mailing-list subscriptions. Returns an empty list if the user does not exist,
     * or if the user has no mailing-list subscriptions.
     * 
     * @param username The given Ef Username
     * @return A 200 OK Response containing the desired user's mailing-list subscriptions.
     */
    @GET
    @Path("/{username}/mailing-list")
    public Response getUserMailingList(@PathParam("username") String username) {
        return Response.ok(profileService.getSubscriptionsByUsername(username)).build();
    }

    /**
     * Returns a 200 OK Response containing the desired user's associated projects. Returns an empty list if the user does not exist, or if
     * the user has no relations to projects.
     * 
     * @param username The given Ef Username
     * @return A 200 OK Response containing the desired user's mailing-list subscriptions.
     */
    @GET
    @Path("/{username}/projects")
    public Response getUserProjects(@PathParam("username") String username) {
        return Response.ok(profileService.getPersonProjects(username)).build();
    }

    /**
     * Initiates the user delete request process. Returns a 201 Created Response containing a list of all newly created
     * UserDeleteRequestData entities. Returns a 404 Not Found if the user cannot be found. Returns a 409 Conflict if the user has already
     * started the deletion process. Returns a 500 Internal Server Error Response if an error occurs during the persistence process.
     * 
     * @param username
     * @return A 201 Created Response containing a list of all newly created UserDeleteRequestData entities. A 404 Not Found if the user
     * cannot be found. Returns a 409 Conflict if the user has already started the deletion process. A 500 Internal Server Error Response if
     * an error occurs during the persistence process.
     */
    @POST
    @Path("/{username}/user_delete_request")
    public Response processUserDeleteRequest(@PathParam("username") String username) {
        Optional<EfUser> user = profileService.getSlimProfileByUsername(username);
        if (user.isEmpty()) {
            throw new NotFoundException(String.format(USER_404_MSG_FORMAT, username));
        }

        // Check current requests via username
        ProfileAPISearchParams params = new ProfileAPISearchParams();
        params.setName(Optional.of(user.get().getName()));
        List<DeleteRequestData> deleteRequests = deleteService.getDeleteRequests(params);
        if (!deleteRequests.isEmpty()) {
            throw new ConflictException(CONFLICT_MSG);
        }

        // Check current requests via user email
        params = new ProfileAPISearchParams();
        params.setMail(Optional.of(user.get().getMail()));
        deleteRequests = deleteService.getDeleteRequests(params);
        if (!deleteRequests.isEmpty()) {
            throw new ConflictException(CONFLICT_MSG);
        }

        // Persist all UserDeleteRequest entities necessary
        List<DeleteRequestData> created = deleteService.createDeleteRequestsForUser(user.get());
        if (created == null || created.isEmpty()) {
            throw new ApplicationException("There was an error while creating delete requests",
                    Status.INTERNAL_SERVER_ERROR.getStatusCode());
        }

        // Persist deletion account request with SysEvent logging
        deleteService.persistAccountRequestWithModLog(user.get());

        // Return all created requests
        return Response.status(Status.CREATED).entity(created).build();
    }
}