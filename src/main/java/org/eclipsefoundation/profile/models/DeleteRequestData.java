/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;
import com.google.auto.value.extension.memoized.Memoized;

/**
 * The public return entity for the user_delete_request DTO. Builds a URL
 * pointing to the current resource.
 */
@AutoValue
@JsonDeserialize(builder = $AutoValue_DeleteRequestData.Builder.class)
public abstract class DeleteRequestData {

    public abstract Long getId();

    public abstract int getUid();

    public abstract String getName();

    public abstract String getMail();

    public abstract String getHost();

    public abstract int getStatus();

    public abstract int getCreated();

    public abstract int getChanged();

    @Memoized
    public String getUrl() {
        return "https://api.eclipse.org/account/user_delete_request/".concat(getId().toString());
    }

    public abstract Builder toBuilder();

    public static Builder builder() {
        return new AutoValue_DeleteRequestData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setId(Long id);

        public abstract Builder setUid(int uid);

        public abstract Builder setName(String name);

        public abstract Builder setMail(String mail);

        public abstract Builder setHost(String host);

        public abstract Builder setStatus(int status);

        public abstract Builder setCreated(int created);

        public abstract Builder setChanged(int changed);

        public abstract DeleteRequestData build();
    }
}
