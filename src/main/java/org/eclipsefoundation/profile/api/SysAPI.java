/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.api;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.foundationdb.client.model.SysModLogData;

import io.quarkus.oidc.client.filter.OidcClientFilter;

/**
 * Fdndb-api binding for System resources. '/sys'
 */
@Path("sys")
@OidcClientFilter
@Produces(MediaType.APPLICATION_JSON)
@RegisterRestClient(configKey = "fdndb-api")
@ApplicationScoped
public interface SysAPI {

    /**
     * Persists SysModLogData entities in foundationDB. Returns a list of
     * created/updated results if the operation was successful.
     * 
     * @param src The new/updated ModLog
     * @return A List or new/updated SysModLogData entities on success.
     */
    @PUT
    @Path("mod_logs")
    @RolesAllowed("fdb_write_sys_modlog")
    List<SysModLogData> insertSysModLog(SysModLogData src);
}