/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.api.models;

import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * The data retrieved from the Gerrit API. Contains only fields relevant to this
 * application.
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_GerritChangeData.Builder.class)
public abstract class GerritChangeData {

    public abstract String getId();

    public abstract String getProject();

    public abstract String getChangeId();

    public abstract String getStatus();

    @JsonProperty("_more_changes")
    @Nullable
    public abstract Boolean getMoreChanges();

    public static Builder builder() {
        return new AutoValue_GerritChangeData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setId(String id);

        public abstract Builder setProject(String project);

        public abstract Builder setChangeId(String id);

        public abstract Builder setStatus(String status);

        @JsonProperty("_more_changes")
        public abstract Builder setMoreChanges(@Nullable Boolean status);

        public abstract GerritChangeData build();
    }
}
