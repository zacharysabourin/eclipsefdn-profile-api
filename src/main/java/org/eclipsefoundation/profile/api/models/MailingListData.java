/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.api.models;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * The data returned from the Mailing-list-API.
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_MailingListData.Builder.class)
public abstract class MailingListData {

    @Nullable
    public abstract String getListName();

    @Nullable
    public abstract String getListDescription();

    public static Builder builder() {
        return new AutoValue_MailingListData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setListName(@Nullable String name);

        public abstract Builder setListDescription(@Nullable String desc);

        public abstract MailingListData build();
    }
}
