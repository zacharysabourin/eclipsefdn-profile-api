/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.api;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.profile.api.models.AccountsProfileData;
import org.jboss.resteasy.util.HttpHeaderNames;

/**
 * Rest client for accounts.eclipse.org. Used to fetch account data via username and uid
 */
@Produces(MediaType.APPLICATION_JSON)
@ApplicationScoped
@RegisterRestClient(configKey = "accounts-profile-api")
public interface DrupalAccountsAPI {

    /**
     * Fetches accounts profile data via username.
     * 
     * @param username The given username
     * @return AccountsProfileData entity if it exists
     */
    @GET
    @Path("{username}")
    AccountsProfileData getAccountsProfileByUsername(@PathParam("username") String username);

    /**
     * Fetches accounts profile data via uid. authentication will be removed once Accounts provides an authenticated way to fetch this data
     * via uid.
     * 
     * @param username The given username
     * @param bearerToken the auth token required for this request
     * @return A list of AccountsProfileData entities if they exist
     */
    @GET
    List<AccountsProfileData> getAccountsProfileByUid(@QueryParam("uid") int uid,
            @HeaderParam(HttpHeaderNames.AUTHORIZATION) String bearerToken);
}
