/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.api;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.foundationdb.client.model.OrganizationContactData;
import org.eclipsefoundation.foundationdb.client.model.OrganizationDocumentData;
import org.eclipsefoundation.foundationdb.client.model.full.FullOrganizationContactData;

import io.quarkus.oidc.client.filter.OidcClientFilter;

/**
 * FoundationDb-API binding for '/organizations'
 */
@Path("/organizations")
@OidcClientFilter
@Produces(MediaType.APPLICATION_JSON)
@ApplicationScoped
@RegisterRestClient(configKey = "fdndb-api")
public interface OrganizationAPI {

    @GET
    @Path("{orgId}/documents")
    @RolesAllowed("fdb_read_organization_documents")
    List<OrganizationDocumentData> getOrganizationDocuments(@PathParam("orgId") String orgId);

    @GET
    @Path("/contacts")
    @RolesAllowed("fdb_read_organization_employment")
    List<OrganizationContactData> getOrgContacts(@QueryParam("personID") String username);

    @GET
    @Path("/contacts/full")
    @RolesAllowed({ "fdb_read_organization_employment", "fdb_read_organization", "fdb_read_people" })
    List<FullOrganizationContactData> getFullOrgContacts(@QueryParam("personID") String username);
}
