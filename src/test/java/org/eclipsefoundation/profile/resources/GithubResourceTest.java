/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.resources;

import java.util.Map;

import org.eclipsefoundation.profile.test.helpers.SchemaNamespaceHelper;
import org.eclipsefoundation.profile.test.helpers.TestNamespaceHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;

import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class GithubResourceTest {

    public static final String PROFILE_BY_HANDLE_URL = "github/profile/{handle}";

    /*
     * BY HANDLE
     */
    public static final EndpointTestCase GET_BY_HANDLE_CASE_SUCCESS_AUTH = TestCaseHelper
            .prepareTestCase(PROFILE_BY_HANDLE_URL, new String[] { TestNamespaceHelper.VALID_HANDLE },
                    SchemaNamespaceHelper.EF_USER_SCHEMA_PATH)
            .setHeaderParams(TestNamespaceHelper.VALID_ANON_AUTH_HEADER)
            .setBodyValidationParams(Map.of("mail", TestNamespaceHelper.VALID_MAIL))
            .build();

    public static final EndpointTestCase GET_BY_HANDLE_SUCCESS_BAD_AUTH = TestCaseHelper
            .prepareTestCase(PROFILE_BY_HANDLE_URL, new String[] { TestNamespaceHelper.VALID_HANDLE },
                    SchemaNamespaceHelper.EF_USER_SCHEMA_PATH)
            .setHeaderParams(TestNamespaceHelper.INVALID_ANON_AUTH_HEADER)
            .setBodyValidationParams(Map.of("mail", ""))
            .build();

    public static final EndpointTestCase GET_BY_HANDLE_CASE_NOT_FOUND = TestCaseHelper
            .prepareTestCase(PROFILE_BY_HANDLE_URL, new String[] { TestNamespaceHelper.INVALID_HANDLE },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
            .setStatusCode(404)
            .setHeaderParams(TestNamespaceHelper.VALID_ANON_AUTH_HEADER).build();

    /*
     * GET /github/profile/{handle}
     */
    @Test
    void testGetUserByHandle_success() {
        EndpointTestBuilder.from(GET_BY_HANDLE_CASE_SUCCESS_AUTH).run();
    }

    @Test
    void testGetUserByHandle_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_BY_HANDLE_CASE_SUCCESS_AUTH).andCheckFormat().run();
    }

    @Test
    void testGetUserByHandle_success_validateSchema() {
        EndpointTestBuilder.from(GET_BY_HANDLE_CASE_SUCCESS_AUTH).andCheckSchema().run();
    }

    @Test
    void testGetUserByHandle_failure_notfound() {
        EndpointTestBuilder.from(GET_BY_HANDLE_CASE_NOT_FOUND).run();
    }

    @Test
    void testGetUserByHandle_failure_notfound_validateSchema() {
        EndpointTestBuilder.from(GET_BY_HANDLE_CASE_NOT_FOUND).andCheckSchema().run();
    }

    @Test
    void testGetUserByHandleBadauth_success() {
        EndpointTestBuilder.from(GET_BY_HANDLE_SUCCESS_BAD_AUTH).run();
    }

    @Test
    void testGetUserByHandleBadAuth_success_validateSchema() {
        EndpointTestBuilder.from(GET_BY_HANDLE_SUCCESS_BAD_AUTH).andCheckSchema().run();
    }

    @Test
    void testGetUserByHandleBadAuth_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_BY_HANDLE_SUCCESS_BAD_AUTH).andCheckFormat().run();
    }
}
