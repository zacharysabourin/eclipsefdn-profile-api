/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.test.helpers;

import java.util.Map;
import java.util.Optional;

import org.eclipsefoundation.profile.models.LdapResult;
import org.jboss.resteasy.util.HttpHeaderNames;

/**
 * Includes all relevant test variables for this application. Includes valid and
 * invalid usernames, uids, emails, GH handles and Auth header key/value pairs.
 * Also includes a mock of the most common LDAP result received while testing.
 */
public final class TestNamespaceHelper {
    public static final String VALID_UID = "666";
    public static final String VALID_USERNAME = "username";
    public static final String VALID_MAIL = "email@fake.com";
    public static final String VALID_HANDLE = "userhandle";
    public static final String VALID_FIRST_NAME = "user";
    public static final String VALID_LAST_NAME = "name";

    public static final String INVALID_HANDLE = "badhandle";
    public static final String INVALID_USERNAME = "nope";
    public static final String INVALID_EMAIL = "email@notfound.com";
    public static final String INVALID_UID = "1";

    public static final String VALID_DELETE_REQUEST_ID = "1";
    public static final String INVALID_DELETE_REQUEST_ID = "600";

    public static final Optional<Map<String, Object>> VALID_ANON_AUTH_HEADER = Optional
            .of(Map.of(HttpHeaderNames.AUTHORIZATION, "Bearer token2"));
    public static final Optional<Map<String, Object>> INVALID_ANON_AUTH_HEADER = Optional
            .of(Map.of(HttpHeaderNames.AUTHORIZATION, "Bearer token3"));
    public static final Optional<Map<String, Object>> VALID_USER_AUTH_HEADER = Optional
            .of(Map.of(HttpHeaderNames.AUTHORIZATION, "Bearer token4"));

    public static final LdapResult VALID_LDAP_RESULT = LdapResult.builder()
            .setUsername(TestNamespaceHelper.VALID_USERNAME)
            .setMail(TestNamespaceHelper.VALID_MAIL)
            .setGithubId(TestNamespaceHelper.VALID_HANDLE)
            .setFirstName(TestNamespaceHelper.VALID_FIRST_NAME)
            .setLastName(TestNamespaceHelper.VALID_LAST_NAME)
            .build();

    private TestNamespaceHelper() {

    }
}
