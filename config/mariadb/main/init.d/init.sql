CREATE DATABASE eclipse;
USE eclipse;

-- eclipse.account_requests definition

CREATE TABLE `account_requests` (
  `email` varchar(100) NOT NULL,
  `new_email` varchar(100) default NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `password` text NOT NULL,
  `ip` char(15) NOT NULL,
  `req_when` datetime default NULL,
  `token` char(64) default NULL,
  `person_id` varchar(255) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE DATABASE eclipse_api;
USE eclipse_api;

-- eclipse_api.api_eclipse_api_gerrit_review_count definition

CREATE TABLE `api_eclipse_api_gerrit_review_count` (
  `uid` int(10) unsigned NOT NULL DEFAULT 0 COMMENT 'The user ID.',
  `review_count` int(11) NOT NULL DEFAULT 0 COMMENT 'Gerrit review count',
  `report_date` int(11) NOT NULL DEFAULT 0 COMMENT 'The Unix timestamp of the report.',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Gerrit review count';

-- eclipse_api.api_eclipse_api_user_delete_request

CREATE TABLE `api_eclipse_api_user_delete_request` (
  `id` serial NOT NULL COMMENT 'Primary Key: Unique ID.',
  `uid` int NOT NULL DEFAULT 0 COMMENT 'The user UID.',
  `name` varchar(60) NOT NULL COMMENT 'The username',
  `mail` varchar(254) NOT NULL COMMENT 'The user email',
  `host` varchar(2048) NOT NULL,
  `status` int NOT NULL DEFAULT 0 COMMENT 'Current status',
  `created` int NOT NULL DEFAULT 0 COMMENT 'Timestamp for when request was created',
  `changed` int NOT NULL DEFAULT 0 COMMENT 'Timestamp for when request was changed',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Track user deletions.';
